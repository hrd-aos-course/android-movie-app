package com.kshrd.androidmovieapp;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.kshrd.androidmovieapp.Adapters.ParentRecyclerViewAdapter;
import com.kshrd.androidmovieapp.Models.Movie;
import com.kshrd.androidmovieapp.Models.ParentModel;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {


    private RecyclerView parentRecyclerView;
    private RecyclerView.Adapter ParentAdapter;
    ArrayList<ParentModel> parentModelArrayList = new ArrayList<>();
    private RecyclerView.LayoutManager parentLayoutManager;

    public static final int NEW_WORD_ACTIVITY_REQUEST_CODE = 1;

    private MovieViewModel mMovieViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //set the Categories for each array list set in the `ParentViewHolder`
        parentModelArrayList.add(new ParentModel("Category1"));
        parentModelArrayList.add(new ParentModel("Category2"));
        parentModelArrayList.add(new ParentModel("Category3"));
        parentModelArrayList.add(new ParentModel("Category4"));
        parentModelArrayList.add(new ParentModel("Category5"));
        parentModelArrayList.add(new ParentModel("Category6"));

        parentRecyclerView = findViewById(R.id.recyclerview);
        parentRecyclerView.setHasFixedSize(true);
        parentLayoutManager = new LinearLayoutManager(this);
        ParentAdapter = new ParentRecyclerViewAdapter(parentModelArrayList, MainActivity.this);
        parentRecyclerView.setLayoutManager(parentLayoutManager);
        parentRecyclerView.setAdapter(ParentAdapter);
        ParentAdapter.notifyDataSetChanged();

//        parentRecyclerView = findViewById(R.id.recyclerview);
//        final MovieListAdapter adapter = new MovieListAdapter(new MovieListAdapter.MovieDiff());
//        parentRecyclerView.setAdapter(adapter);
//        parentRecyclerView.setLayoutManager(new LinearLayoutManager(this));
//
//        // Get a new or existing ViewModel from the ViewModelProvider.
//        mMovieViewModel = new ViewModelProvider(this).get(MovieViewModel.class);
//
//        mMovieViewModel.getAllWords().observe(this, words -> {
//            // Update the cached copy of the words in the adapter.
//            adapter.submitList(words);
//        });

    }

}