package com.kshrd.androidmovieapp.Models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "category_table")
public class Category {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "category")
    public long categoryId;
    public String title;
    private String movieCategory;

    public String movieCategory() {
        return movieCategory;
    }

    public Category(long categoryId, String title) {
        this.categoryId = categoryId;
        this.title = title;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public String getTitle() {
        return title;
    }
}
