package com.kshrd.androidmovieapp.Models;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "movie_table")
public class Movie {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "movie")
    public String mMovie;

    //public long categoryCreatorId;

    public Movie(@NonNull String movie) {
        this.mMovie = movie;

    }

    @NonNull
    public String getMovie() {
        return mMovie;
    }
}
