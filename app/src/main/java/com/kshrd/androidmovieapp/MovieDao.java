package com.kshrd.androidmovieapp;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.kshrd.androidmovieapp.Models.Movie;

import java.util.List;

@Dao
public interface MovieDao {
    @Query("SELECT * FROM movie_table ORDER BY movie ASC")
    LiveData<List<Movie>> getAlphabetizedMovies();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Movie movie);

    @Query("DELETE FROM movie_table")
    void deleteAll();
}
