package com.kshrd.androidmovieapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MovieViewHolder extends RecyclerView.ViewHolder {

    private final TextView wordItemView;

    public MovieViewHolder(View itemView) {
        super(itemView);
        wordItemView = itemView.findViewById(R.id.movie_name);
    }

    public void bind(String text) {
        wordItemView.setText(text);
    }

    static MovieViewHolder create(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_item, parent, false);
        return new MovieViewHolder(view);
    }
}
